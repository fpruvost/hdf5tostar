/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads HDF5 files with several MPI processes.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <libgen.h>
#include <hdf5.h>
#include <mpi.h>

#define MAXCHAR 1000
#define DIMS 2

int max(int num1, int num2);
int min(int num1, int num2);
float timedifference_msec(struct timeval t0, struct timeval t1);

int main(int argc, char *argv[]) {
    int rank, nprocs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (argc < 2){
        if (rank == 0){
            printf("Please give: \n");
            printf("1. the path to an hdf5 metadata file .txt\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    char *filename = malloc(strlen(argv[1]) + 1);
    if (filename != NULL) {
        strcpy(filename, argv[1]);
    } else {
        MPI_Finalize();
        exit(-1);
    }

    /* Read the metadata */
    printf("%d: read txt file %s\n", rank, filename);
    FILE *datafile = fopen(filename, "r");
    if (datafile == NULL){
        printf("Could not open file %s", filename);
        MPI_Finalize();
        exit(-1);
    }

    char str[MAXCHAR];
    fgets(str, MAXCHAR, datafile);
    // number of blocks, in rows and columns, e.g. 3 means the global matrix is
    // made of 3x3 blocks
    int blocknumber1d = atoi(str);
    //printf("%d: blocknumber1d %d\n", rank, blocknumber1d);
    fgets(str, MAXCHAR, datafile);
    // upper/lower part of the matrix the blocks represent
    char format[MAXCHAR];
    strcpy(format, str);
    //printf("%d: format %s", rank, format);
    int nblocks = blocknumber1d*(blocknumber1d+1)/2;
    if( format == "full" ){
        nblocks = blocknumber1d*blocknumber1d;
    }
    printf("%d: nblocks %d\n", rank, nblocks);

    char h5filenames[nblocks][MAXCHAR];
    hid_t file_id[nblocks];
    hid_t dataset_id[nblocks];
    hid_t dspace_id[nblocks];
    hid_t memspace_id[nblocks];
    hsize_t start[nblocks][DIMS];
    hsize_t stride[nblocks][DIMS];
    hsize_t count[nblocks][DIMS];
    hsize_t block[nblocks][DIMS];
    double **dset_data = malloc( nblocks*sizeof(double *) );

    /* Read HDF5 files metadata */
    const char delimiter[] = "\t";
    for (int i=0; i < nblocks; i++){
        /* Read metadata */
        fgets(str, MAXCHAR, datafile);
        char *tmp = strtok(str, delimiter);
        int index_i = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        int index_j = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        strcpy(&h5filenames[i][0], tmp);
        //printf("%d: %d %d %s\n", rank, index_i, index_j, h5filenames[i]);
        /* conversion filename to absolute path expecting H5 files in the
        same directory than the txt file */
        tmp = strdup(filename);
        char *pathtofile = dirname(tmp);
        //printf("%d: read H5 file in %s\n", rank, pathtofile);
        char myfile[MAXCHAR];
        strcpy(myfile, pathtofile);
        strcat(myfile, "/");
        strcat(myfile, &h5filenames[i][0]);
        strcpy(&h5filenames[i][0], myfile);
        /* Reading header to get dimensions. */
        //printf("%d: read H5 file header %s\n", rank, &h5filenames[i][0]);
        if( access( &h5filenames[i][0], F_OK ) != -1 ) {
            file_id[i] = H5Fopen(&h5filenames[i][0], H5F_ACC_RDONLY, H5P_DEFAULT);
            /* Get dimensions. */
            dataset_id[i] = H5Dopen2(file_id[i], "/distance", H5P_DEFAULT);
            dspace_id[i] = H5Dget_space(dataset_id[i]);
            //printf("%d: file_id %ld dataset_id %ld dspace_id %ld\n", rank, file_id[i], dataset_id[i], dspace_id[i]);
            hsize_t dims[DIMS];
            H5Sget_simple_extent_dims(dspace_id[i], dims, NULL);
            //printf("%d: dims %llu %llu\n", rank, dims[0], dims[1]);

            /* Define the data structure to read for each process */
            hsize_t nrows = dims[0] / nprocs;
            hsize_t ncols = dims[1];
            hsize_t offsetrows = rank*nrows;
            if ( rank == nprocs-1 ){
                nrows = dims[0] - (nprocs-1)*nrows;
            }
            start[i][0]  = offsetrows;
            start[i][1]  = 0;
            stride[i][0] = 1;
            stride[i][1] = 1;
            count[i][0]  = nrows;
            count[i][1]  = ncols;
            block[i][0]  = 1;
            block[i][1]  = 1;

            /* create memspace */
            memspace_id[i] = H5Screate_simple (DIMS, &count[i][0], NULL);
            /* select subset */
            H5Sselect_hyperslab (dspace_id[i], H5S_SELECT_SET, &start[i][0], &stride[i][0], &count[i][0], &block[i][0]);
            /* allocate buffer to receive data */
            hsize_t buffersize = nrows*ncols;
            dset_data[i] = (double*) malloc( buffersize*sizeof(double) );
            //printf("%d: file %s dims %lld %lld\n", rank, &h5filenames[i][0], nrows, ncols);
        } else {
            printf("Could not access file %s", &h5filenames[i][0]);
            MPI_Finalize();
            exit(-1);
        }
    }

    float elapsed_cumul = 0.;
    for (int i=0; i < nblocks; i++){
        /* Read dataset's values */
        if (dset_data[i]){
            printf("%d: read H5 file %s - subset %lld %lld %lld %lld\n", rank, &h5filenames[i][0], start[i][0], start[i][0]+count[i][0], start[i][1], start[i][1]+count[i][1]);
            struct timeval tstop, tstart;
            float elapsed;
            gettimeofday(&tstart, NULL);
            H5Dread(dataset_id[i], H5T_IEEE_F64LE, memspace_id[i], dspace_id[i], H5P_DEFAULT, &dset_data[i][0]);
            gettimeofday(&tstop, NULL);
            elapsed = timedifference_msec(tstart, tstop);
            elapsed_cumul += elapsed;
            //printf("%d: H5Dread - Took : %f s\n", rank, elapsed/1000);
            //printf("%d: 1st values: %f %f\n", rank, dset_data[i][0], dset_data[i][1]);
        }
    }
    printf("%d: H5Dread cumulated - Took : %f s\n", rank, elapsed_cumul/1000);

    for (int i=0; i < nblocks; i++){
        /* Close the H5 dataspace. */
        H5Sclose (dspace_id[i]);
        /* Close the H5 dataset. */
        H5Dclose(dataset_id[i]);
        /* Close the H5 file. */
        H5Fclose(file_id[i]);
        free(dset_data[i]);
    }
    free(dset_data);
    /* Close the metadata .txt file */
    fclose(datafile);
    /* MPI finalize */
    MPI_Finalize();
    /* return status */
    return 0;
}
/**
 * Find maximum between two numbers.
 */
int max(int num1, int num2)
{
    return (num1 > num2 ) ? num1 : num2;
}
/**
 * Find minimum between two numbers.
 */
int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}
float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}