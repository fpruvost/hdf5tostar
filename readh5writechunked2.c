/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads an HDF5 file then write a new file and dump data into it
 *  without compression.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <hdf5.h>

int main(int argc, char *argv[]) {

    hid_t       file_id, dataset_id, dataspace_id, plist_id;  /* identifiers */
    hsize_t     cdims[2];
    herr_t      status;
    char        *FILEIN, *FILEOUT;
    int         chunk;
    double      *dset_data;
    clock_t     start, stop;

    if (argc < 4){
        printf("Please give the path to an existing HDF5 file and a path to the file to create as argument to this executable as well as the chunk size\n");
        return 0;
    }

    FILEIN  = malloc(strlen(argv[1]) + 1);
    if (FILEIN != NULL) {
        strcpy(FILEIN, argv[1]);
    } else {
        return -1;
    }

    printf("Reading file: %s\n", FILEIN);

    /* Open an existing file. */
    file_id = H5Fopen(FILEIN, H5F_ACC_RDONLY, H5P_DEFAULT);

    /* Open an existing dataset. */
    dataset_id = H5Dopen2(file_id, "/distance", H5P_DEFAULT);

    hid_t dspace = H5Dget_space(dataset_id);
    const int ndims = H5Sget_simple_extent_ndims(dspace);
    hsize_t dims[ndims];
    H5Sget_simple_extent_dims(dspace, dims, NULL);
    printf("Dims: %llu %llu\n", dims[0], dims[1]);

    /* Read dataset's values */
    dset_data = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
    start = clock();
    status = H5Dread(dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, dset_data);
    stop = clock();
    printf("Took : %f s\n", (double)(stop - start) / CLOCKS_PER_SEC);
    printf("1st values: %f %f\n", dset_data[0], dset_data[1]);

    /* Close the dataset. */
    status = H5Dclose(dataset_id);

    /* Close the file. */
    status = H5Fclose(file_id);


    /* Create a new file and write data into it without any compression */
    FILEOUT  = malloc(strlen(argv[2]) + 1);
    if (FILEOUT != NULL) {
        strcpy(FILEOUT, argv[2]);
    } else {
        return -1;
    }
    chunk = atoi(argv[3]);
    printf("Writing file: %s with chunk (1, %d)\n", FILEOUT, chunk);

    /* Create a new file using default properties. */
    file_id = H5Fcreate(FILEOUT, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    plist_id = H5Pcreate(H5P_DATASET_CREATE);

    /* Dataset must be chunked for compression */
    cdims[0] = 1;
    cdims[1] = chunk;
    status = H5Pset_chunk(plist_id, 2, cdims);

    /* Set ZLIB / DEFLATE Compression using compression level 5. */
    status = H5Pset_deflate(plist_id, 5);

    /* Create the data space for the dataset. */
    dataspace_id = H5Screate_simple(2, dims, NULL);

    /* Create the dataset. */
    dataset_id = H5Dcreate2(file_id, "/distance", H5T_IEEE_F64LE,
                            dataspace_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);

    /* Write the dataset. */
    status = H5Dwrite(dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                      dset_data);

    /* Terminate access to the data space. */
    status = H5Sclose(dataspace_id);

    /* End access to the dataset and release resources used by it. */
    status = H5Dclose(dataset_id);

    /* Terminate access to properties */
    status = H5Pclose(plist_id);

    /* Close the file. */
    status = H5Fclose(file_id);

    free(FILEIN);
    free(FILEOUT);
    free(dset_data);

    return status;
}
