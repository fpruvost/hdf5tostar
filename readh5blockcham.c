/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads an HDF5 file first in sequential then in parallel by
 *  sub-blocks of the matrix using Chameleon.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include<libgen.h>
#include <hdf5.h>
#include <mpi.h>
#include <chameleon.h>

#define MAXCHAR 1000
#define DIMS 2

typedef struct{
    // number of blocks, in rows and columns, e.g. 3 means the global
    // matrix is made of 3x3 blocks
    int blockNumber1D;
    // upper/lower part of the matrix the blocks represent
    char format[MAXCHAR];
    // number of blocks to read
    // this depends on the format UPPER, LOWER, FULL
    // e.g. blockNumber1D = 3, format = UPPER then blockNumber = 6
    int blockNumber;
    // HDF5 filename of each block
    char **h5FileNames;
    // HDF5 datasets to consider for each block
    hid_t *dataSets;
    // dimensions (rows, columns) of the global matrix
    hsize_t **dims;
    // global indexes (i,j) of the first element of each block
    int **indexStart;
    // global indexes (i,j) of the last element of each block
    int **indexEnd;
} Hdf5Blocks;

static int chamMapHdf5Tile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    );

int min(int num1, int num2);
float timedifference_msec(struct timeval t0, struct timeval t1);

int main(int argc, char *argv[]) {
    int rank, nprocs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (argc < 3){
        if (rank == 0){
            printf("Please give: \n");
            printf("1. the path to an hdf5 metadata file .txt\n");
            printf("2. the number of threads\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    char *filename = malloc(strlen(argv[1]) + 1);
    if (filename != NULL) {
        strcpy(filename, argv[1]);
    } else {
        MPI_Finalize();
        exit(-1);
    }
    int nthreads = atoi(argv[2]);

    // structure that wraps the global matrix by blocks information
    Hdf5Blocks h5blocks;

    /* Read the metadata */
    printf("%d: read file %s\n", rank, filename);
    FILE *datafile = fopen(filename, "r");
    if (datafile == NULL){
        printf("Could not open file %s", filename);
        return 1;
    }

    char str[MAXCHAR];
    fgets(str, MAXCHAR, datafile);
    h5blocks.blockNumber1D = atoi(str);
    //printf("%d: blockNumber1D %d\n", rank, h5blocks.blockNumber1D);
    fgets(str, MAXCHAR, datafile);
    strcpy(h5blocks.format, str);
    //printf("%d: format %s", rank, h5blocks.format);
    int nbBlocks = h5blocks.blockNumber1D*(h5blocks.blockNumber1D+1)/2;
    if(h5blocks.format == "full"){
        nbBlocks = h5blocks.blockNumber1D*h5blocks.blockNumber1D;
    }
    h5blocks.blockNumber = nbBlocks;
    if ( nbBlocks < nprocs ){
        if (rank ==0){
            printf("Number of files to read is lower than number of processes\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    //printf("%d: blockNumber %d\n", rank, h5blocks.blockNumber);
    h5blocks.h5FileNames = malloc(nbBlocks * sizeof(char *));
    const char delimiter[] = "\t";
    for (int i=0; i < nbBlocks; i++){
        fgets(str, MAXCHAR, datafile);
        char *tmp = strtok(str, delimiter);
        int index_i = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        int index_j = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        h5blocks.h5FileNames[i] = (char *)malloc(MAXCHAR+1);
        strcpy(h5blocks.h5FileNames[i], tmp);
        //printf("%d: %d %d %s\n", rank, index_i, index_j, h5blocks.h5FileNames[i]);
    }

    /* Open an existing file. */
    char *tmp = strdup(filename);
    char *pathtofile = dirname(tmp);
    printf("%d: %s\n", rank, pathtofile);
    char myfile[MAXCHAR];
    strcpy(myfile, pathtofile);
    strcat (myfile, "/");
    strcat (myfile, h5blocks.h5FileNames[rank]);
    printf("%d: %s\n", rank, myfile);
    hid_t file_id = H5Fopen(myfile, H5F_ACC_RDONLY, H5P_DEFAULT);

    /* Open an existing dataset. */
    hid_t dataset_id = H5Dopen2(file_id, "/distance", H5P_DEFAULT);
    hid_t dspace_id = H5Dget_space(dataset_id);
    const int ndims=2;
    hsize_t dims[ndims];
    H5Sget_simple_extent_dims(dspace_id, dims, NULL);
    printf("%d: dims %llu %llu\n", rank, dims[0], dims[1]);

    /* init chameleon and read hdf5 data with map */
    CHAMELEON_Init( nthreads, 0 );
    int nb;
    CHAMELEON_Get( CHAMELEON_TILE_SIZE, &nb );
    CHAM_desc_t  *cham_desc=NULL;
    CHAMELEON_Desc_Create(&cham_desc, NULL, ChamRealDouble, nb, nb, nb*nb, dims[0], dims[1], 0, 0, dims[0], dims[1], nprocs, 1);

    struct timeval stop, start;
    float elapsed;
    gettimeofday(&start, NULL);
    CHAMELEON_map_Tile( ChamUpperLower, cham_desc, chamMapHdf5Tile, &dataset_id );
    gettimeofday(&stop, NULL);
    elapsed = timedifference_msec(start, stop);
    if (rank == 0) printf("Multithread Chameleon one file per MPI process - Took : %f ms\n", elapsed);

    /* Deallocate Chameleon matrix */
    CHAMELEON_Desc_Destroy( &cham_desc );
    /* Chameleon finalize */
    CHAMELEON_Finalize();
    /* Close the H5 dataset. */
    H5Dclose(dataset_id);
    /* Close the H5 file. */
    H5Fclose(file_id);
    /* Close the metadata .txt file */
    fclose(datafile);
    /* MPI finalize */
    MPI_Finalize();
    /* return status */
    return 0;
}

static int chamMapHdf5Tile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    ){
    // parameters for tile dimension and position in the global matrix
    size_t nrows, ncols, ldam, row_min, col_min;
    // parameters to select the subset
    hsize_t start[2], stride[2], count[2], block[2];
    // we receive data from HDF5 in row major format
    double *tilerm;
    // address of the tile to fill
    double *T = (double*)cham_tile->mat;
    // metadata to copy the dataset blocks (pointer to the dataset_id)
    hid_t *dset = (hid_t*)user_data;

    // uplo is meaningfull only for tiles on the diagonal, else we need to copy
    // the entire tile. For now only ChamUpperLower is compatible in this
    // function. TODO: handle Upper and Lower storage.
    uplo = ( m == n ) ? uplo : ChamUpperLower;

    // sizes of the tile
    nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
    ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
    ldam  = descA->get_blkldd( descA, m );
    // starting row index of the block to copy from A
    row_min = m * descA->mb;
    // starting column index of the block to copy from A
    col_min = n * descA->nb;

    // HDF5 parameters for loading a specific block
    start[0] = row_min;
    start[1] = col_min;
    stride[0] = 1;
    stride[1] = 1;
    count[0] = nrows;
    count[1] = ncols;
    block[0] = 1;
    block[1] = 1;

    // create memspace
    hid_t memspace = H5Screate_simple (2, count, NULL);
    // get dataspace of the dataset
    hid_t dspace = H5Dget_space(*dset);
    const int ndims = H5Sget_simple_extent_ndims(dspace);
    hsize_t dims[ndims];
    H5Sget_simple_extent_dims(dspace, dims, NULL);
    // select subset
    H5Sselect_hyperslab (dspace, H5S_SELECT_SET, start, stride, count, block);

    // read and copy the block into the tile in row major (HDF5 convention)
    tilerm = (double*) malloc( nrows*ncols*sizeof(double) );
    H5Dread (*dset, H5T_IEEE_F64LE, memspace, dspace, H5P_DEFAULT, tilerm);

    // Turn in column major
    for (size_t j = 0; j < ncols; ++j){
        size_t imin = ( uplo == ChamLower ) ? j               : 0;
        size_t imax = ( uplo == ChamUpper ) ? min(j+1, nrows) : nrows;
        for (size_t i = imin; i < imax; ++i){
            T[ j*ldam + i ] = tilerm[ i*ncols + j ];
        }
    }

    free(tilerm);
    H5Sclose (dspace);

    return 0;
}

/**
 * Find minimum between two numbers.
 */
int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}

float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}
