/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads HDF5 files with several MPI processes.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <libgen.h>
#include <hdf5.h>
#include <mpi.h>

#define MAXCHAR 1000
#define DIMS 2
#define INTERVAL 2
#define TOLERANCE 5
#define TILESIZE 320

int max(int num1, int num2);
int min(int num1, int num2);
float timedifference_msec(struct timeval t0, struct timeval t1);

typedef struct MpiRanks MpiRanks;
/**
 * @brief Mpi ranks handling H5 File
 */
struct MpiRanks{
    int rank;
    MpiRanks *next;
};
typedef struct IdxInt IdxInt;
/**
 * @brief Indexes intervals (or rows/columns) handled by processes in H5 Files
 */
struct IdxInt{
    hsize_t idx[INTERVAL];
    IdxInt *next;
};
typedef struct MapH5MpiList MapH5MpiList;
struct MapH5MpiList{
    int nb;
    MpiRanks *firstmpiranks;
    IdxInt *firstrows;
    IdxInt *firstcols;
};
/**
 * @brief Define the subset of MPI processes handling the HDF5 file.
 */
struct MapH5Mpi{
    char filename[MAXCHAR]; // H5 filename, absolute path
    int npmax;              // number max of different processes handling the file
    MapH5MpiList *mpirr; // list of MPI ranks handling the file and of rows interval to handle in the file for each MPI rank
};

int main(int argc, char *argv[]) {
    int rank, nprocs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    const char* tilesize_s = getenv("H5TS_TILE_SIZE");
    int tilesize = TILESIZE;
    if ( tilesize_s != NULL ){
        tilesize = atoi(tilesize_s);
    }
    if (rank ==0) printf("%d: tilesize %d\n", rank, tilesize);

    if (argc < 2){
        if (rank == 0){
            printf("Please give: \n");
            printf("1. the path to an hdf5 metadata file .txt\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    char *filename = malloc(strlen(argv[1]) + 1);
    if (filename != NULL) {
        strcpy(filename, argv[1]);
    } else {
        MPI_Finalize();
        exit(-1);
    }

    /* Read the metadata */
    printf("%d: read txt file %s\n", rank, filename);
    FILE *datafile = fopen(filename, "r");
    if (datafile == NULL){
        printf("Could not open file %s", filename);
        MPI_Finalize();
        exit(-1);
    }

    char str[MAXCHAR];
    fgets(str, MAXCHAR, datafile);
    // number of blocks, in rows and columns, e.g. 3 means the global matrix is
    // made of 3x3 blocks
    int blocknumber1d = atoi(str);
    //printf("%d: blocknumber1d %d\n", rank, blocknumber1d);
    fgets(str, MAXCHAR, datafile);
    // upper/lower part of the matrix the blocks represent
    char format[MAXCHAR];
    strcpy(format, str);
    //printf("%d: format %s", rank, format);
    int nblocks = blocknumber1d*(blocknumber1d+1)/2;
    if( format == "full" ){
        nblocks = blocknumber1d*blocknumber1d;
    }
    printf("%d: nblocks %d\n", rank, nblocks);

    char h5filenames[nblocks][MAXCHAR];
    int h5fileloc[nblocks][DIMS];
    hid_t file_id[nblocks];
    hid_t dataset_id[nblocks];
    hid_t dspace_id[nblocks];
    hsize_t dims[nblocks][DIMS];

    /* Read HDF5 files metadata to get dimensions */
    const char delimiter[] = "\t";
    for (int i=0; i < nblocks; i++){
        /* Read metadata */
        fgets(str, MAXCHAR, datafile);
        char *tmp = strtok(str, delimiter);
        h5fileloc[i][0] = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        h5fileloc[i][1] = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        strcpy(&h5filenames[i][0], tmp);
        //printf("%d: %d %d %s\n", rank, index_i, index_j, h5filenames[i]);
        /* conversion filename to absolute path expecting H5 files in the
        same directory than the txt file */
        tmp = strdup(filename);
        char *pathtofile = dirname(tmp);
        //printf("%d: read H5 file in %s\n", rank, pathtofile);
        char myfile[MAXCHAR];
        strcpy(myfile, pathtofile);
        strcat(myfile, "/");
        strcat(myfile, &h5filenames[i][0]);
        strcpy(&h5filenames[i][0], myfile);
        /* Reading header to get dimensions. */
        //printf("%d: read H5 file header %s\n", rank, &h5filenames[i][0]);
        if( access( &h5filenames[i][0], F_OK ) != -1 ) {
            file_id[i] = H5Fopen(&h5filenames[i][0], H5F_ACC_RDONLY, H5P_DEFAULT);
            /* Get dimensions. */
            dataset_id[i] = H5Dopen2(file_id[i], "/distance", H5P_DEFAULT);
            dspace_id[i] = H5Dget_space(dataset_id[i]);
            //printf("%d: file_id %ld dataset_id %ld dspace_id %ld\n", rank, file_id[i], dataset_id[i], dspace_id[i]);
            H5Sget_simple_extent_dims(dspace_id[i], &dims[i][0], NULL);
            //printf("%d: dims[%d] %llu %llu\n", rank, i, dims[i][0], dims[i][1]);
        } else {
            printf("Could not access file %s", &h5filenames[i][0]);
            MPI_Finalize();
            exit(-1);
        }
    }
    free(filename);

    hsize_t idxh5glob[blocknumber1d][DIMS]; // index, min/max, of blocks in the global matrix
    idxh5glob[0][0] = 0;
    idxh5glob[0][1] = dims[0][1] - 1;
    for (int i=1; i < blocknumber1d; i++){
        idxh5glob[i][0] = idxh5glob[i-1][1] + 1;
        idxh5glob[i][1] = idxh5glob[i][0] + dims[i][1] - 1;
    }
    //if (rank == 0){
    //    for (int i=0; i < blocknumber1d; i++){
    //        printf("%d: idxh5glob %llu %llu\n", rank, idxh5glob[i][0], idxh5glob[i][1]);
    //    }
    //}

    /* Compute total number of elements over all the blocks. */
    hsize_t dimsglob = 0;
    for (int i=0; i<nblocks; i++){
        dimsglob += dims[i][0] * dims[i][1];
    }
    if (rank == 0) printf("%d: dimsglob %llu\n", rank, dimsglob);

    hsize_t nbvalperprocref = dimsglob / nprocs ;
    hsize_t nbvaltolerance = nbvalperprocref*TOLERANCE/(float)100;
    if (rank == 0) printf("%d: Average number of elements per process so that it is balanced %llu tolerance +- %llu\n", rank, nbvalperprocref, nbvaltolerance);
    if (nbvalperprocref == 0){
        printf("Matrix is so small that we don't have the minimum of one value per process to read, please decrease the number of processes or consider a larger matrix.\n");
        MPI_Finalize();
        exit(-1);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    /* Hdf5 files <-> MPI ranks allocation */

    /* Initialize structures */
    struct MapH5Mpi maph5mpi[nblocks];
    for (int i=0; i<nblocks; i++){
        strcpy(maph5mpi[i].filename, &h5filenames[i][0]);
        maph5mpi[i].npmax = 1;
        maph5mpi[i].mpirr = malloc( sizeof(MapH5MpiList) );
        maph5mpi[i].mpirr->nb = 0;
        MpiRanks *mpiranks = malloc( sizeof(MpiRanks) );
        mpiranks->rank = 0;
        mpiranks->next = NULL;
        maph5mpi[i].mpirr->firstmpiranks = mpiranks;
        IdxInt *rows = malloc( sizeof(IdxInt) );
        rows->idx[0] = 0;
        rows->idx[1] = 0;
        rows->next = NULL;
        maph5mpi[i].mpirr->firstrows = rows;
        IdxInt *cols = malloc( sizeof(IdxInt) );
        cols->idx[0] = 0;
        cols->idx[1] = 0;
        cols->next = NULL;
        maph5mpi[i].mpirr->firstcols = cols;
    }
    hsize_t nbrowsh5remain[nblocks];  // number of remaining rows to handle in each file
    hsize_t nbrowsh5remainglob = 0;   // number of remaining rows considering all files
    hsize_t rowsidxh5[nblocks][DIMS]; // index of rows, min/max, for allocations
    hsize_t colsidxh5[nblocks][DIMS]; // index of cols, min/max, for allocations
    for (int i=0; i<nblocks; i++){
        rowsidxh5[i][0] = 0;
        colsidxh5[i][0] = 0;
        //if (rank == 0) printf("%d: h5fileloc %d %d\n", rank, h5fileloc[i][0], h5fileloc[i][1]);
        if ( h5fileloc[i][0] > 0 ){
            hsize_t remaindertile = idxh5glob[h5fileloc[i][0]][0] % tilesize;
            if ( remaindertile != 0 ){
                rowsidxh5[i][0] = tilesize - remaindertile;
            }
            //if (rank == 0) printf("%d: %lld %d %lld\n", rank, idxh5glob[h5fileloc[i][0]][0], tilesize, rowsidxh5[i][0]);
        }
        if ( h5fileloc[i][1] > 0 && h5fileloc[i][1] > h5fileloc[i][0] ){
            hsize_t remaindertile = idxh5glob[h5fileloc[i][1]][0] % tilesize;
            if ( remaindertile != 0 ){
                colsidxh5[i][0] = tilesize - remaindertile;
            }
            //if (rank == 0) printf("%d: %lld %d %lld\n", rank, idxh5glob[h5fileloc[i][1]][0], tilesize, colsidxh5[i][0]);
        }
        rowsidxh5[i][1] = rowsidxh5[i][0]-1;
        colsidxh5[i][1] = dims[i][1]-1;
        nbrowsh5remain[i] = dims[i][0] - rowsidxh5[i][1] - 1;
        nbrowsh5remainglob += dims[i][0];
        //if (rank == 0) printf("%d: rowsidxh5 %lld %lld colsidxh5 %lld %lld %lld\n", rank, rowsidxh5[i][0], rowsidxh5[i][1], colsidxh5[i][0], colsidxh5[i][1], nbrowsh5remain[i]);
    }

    hsize_t nbvalproc[nprocs]; // load on each proc
    for (int i=0; i<nprocs; i++){
        nbvalproc[i] = 0;
    }

    hsize_t nbrowsblock[nblocks]; // number of rows in each sub-block
    for (int i=0; i<nblocks; i++){
        hsize_t nbvaltile = tilesize * tilesize;
        if (dims[i][1] >=  nbvaltile){
            nbrowsblock[i] = 1;
        } else {
            nbrowsblock[i] = (nbvaltile / dims[i][1]) + 1;
        }
        //if (rank == 0) printf("%d: nbrowsblock %lld\n", rank, nbrowsblock[i]);
    }

    int iter = 0;
    /* Main loop for allocation */
    while ( nbrowsh5remainglob > 0 ){
        iter++;
        //if (iter==20) exit(0);

        /* search the most loaded H5 block */
        int bidx = 0;
        hsize_t blockloadmax = nbrowsh5remain[0]*dims[0][1];
        for (int i=1; i < nblocks; i++){
            hsize_t blockload = nbrowsh5remain[i]*dims[i][1];
            if ( blockload > blockloadmax ){
                blockloadmax = blockload;
                bidx = i;
            }
        }

        /* search the least loaded proc */
        int pidx = 0;
        hsize_t procloadmin = nbvalproc[0];
        for (int i=1; i < nprocs; i++){
            hsize_t procload = nbvalproc[i];
            if ( procload < procloadmin){
                procloadmin = procload;
                pidx = i;
            }

        }

        /* load proc with H5 block rows */
        hsize_t ncolsblock = colsidxh5[bidx][1] - colsidxh5[bidx][0] + 1;
        hsize_t nbrowstofillproc = max(1, (nbvalperprocref - nbvalproc[pidx]) / ncolsblock + 1);
        hsize_t nbrowsproctoadd  = min(nbrowsh5remain[bidx], nbrowstofillproc);

        /* if remaining number of rows is very small we accept to handle it to
        avoid getting new processes handling this file */
        if ( (nbrowsh5remain[bidx] - nbrowsproctoadd)*ncolsblock < nbvaltolerance ){
            nbrowsproctoadd = nbrowsh5remain[bidx];
        }
        //if (rank == 0) printf("%d %d %lld %lld %lld %lld %lld %lld %lld\n", bidx, pidx, nbrowsh5remainglob, nbrowsh5remain[bidx], nbvalproc[pidx], nbrowstofillproc, nbrowsproctoadd, dims[bidx][0], rowsidxh5[bidx][0]);

        nbvalproc[pidx]      += nbrowsproctoadd * ncolsblock;
        nbrowsh5remain[bidx] -= nbrowsproctoadd;
        nbrowsh5remainglob   -= nbrowsproctoadd;
        //if (rank == 0) printf("remove nbrowsh5remainglob from %d nbrowsproctoadd %lld\n", bidx, nbrowsproctoadd);
        rowsidxh5[bidx][1]   += nbrowsproctoadd;

        /* new allocation for this H5 block */
        hsize_t mt = (rowsidxh5[bidx][1] - rowsidxh5[bidx][0]) / nbrowsblock[bidx] + 1;
        for (int i=0; i < mt; i++){
            MpiRanks *mpiranks = malloc( sizeof(MpiRanks) );
            mpiranks->rank = pidx;
            mpiranks->next = NULL;
            MpiRanks *ptrrank = maph5mpi[bidx].mpirr->firstmpiranks;
            while(ptrrank->next!=NULL){
                ptrrank = ptrrank->next;
            }
            ptrrank->next = mpiranks;
            IdxInt *rows = malloc( sizeof(IdxInt) );
            rows->idx[0] = rowsidxh5[bidx][0]+i*nbrowsblock[bidx];
            rows->idx[1] = min(rowsidxh5[bidx][0]+(i+1)*nbrowsblock[bidx] -1, rowsidxh5[bidx][1]);
            rows->next = NULL;
            IdxInt *ptridx = maph5mpi[bidx].mpirr->firstrows;
            while(ptridx->next!=NULL){
                ptridx = ptridx->next;
            }
            ptridx->next = rows;
            IdxInt *cols = malloc( sizeof(IdxInt) );
            cols->idx[0] = colsidxh5[bidx][0];
            cols->idx[1] = colsidxh5[bidx][1];
            cols->next = NULL;
            ptridx = maph5mpi[bidx].mpirr->firstcols;
            while(ptridx->next!=NULL){
                ptridx = ptridx->next;
            }
            ptridx->next = cols;
            maph5mpi[bidx].mpirr->nb += 1;
            //if (rank == 0) printf("alloc %d %d %lld %lld %lld %lld\n", bidx, pidx, rows->idx[0], rows->idx[1], cols->idx[0], cols->idx[1]);
        }

        /* allocate the tile overlap over columns on the next H5 file on my right */
        //if (rank == 0) printf("overlap col %d %d\n", bidx, h5fileloc[bidx][1]);
        if ( h5fileloc[bidx][1] < blocknumber1d-1 )
        {
            hsize_t nbcolsoverlapint = (idxh5glob[h5fileloc[bidx][1]][1]+1)%tilesize;
            //if (rank == 0) printf("nbcolsoverlapint %lld %lld\n", nbcolsoverlapint, idxh5glob[h5fileloc[bidx][1]][1]);
            if (nbcolsoverlapint > 0) {
                int bidxoverlap = bidx+1;
                hsize_t nbcolsoverlapext = tilesize - nbcolsoverlapint;
                MpiRanks *mpiranksoverlap = malloc( sizeof(MpiRanks) );
                mpiranksoverlap->rank = pidx;
                mpiranksoverlap->next = NULL;
                MpiRanks *ptrrank=maph5mpi[bidxoverlap].mpirr->firstmpiranks;
                while(ptrrank->next!=NULL){
                    ptrrank = ptrrank->next;
                }
                ptrrank->next = mpiranksoverlap;
                IdxInt *rowsoverlap = malloc( sizeof(IdxInt) );
                rowsoverlap->idx[0] = rowsidxh5[bidx][0];
                rowsoverlap->idx[1] = rowsidxh5[bidx][1];
                rowsoverlap->next = NULL;
                IdxInt *ptridx = maph5mpi[bidxoverlap].mpirr->firstrows;
                while(ptridx->next!=NULL){
                    ptridx = ptridx->next;
                }
                ptridx->next = rowsoverlap;
                IdxInt *colsoverlap = malloc( sizeof(IdxInt) );
                colsoverlap->idx[0] = 0;
                colsoverlap->idx[1] = nbcolsoverlapext-1;
                colsoverlap->next = NULL;
                ptridx = maph5mpi[bidxoverlap].mpirr->firstcols;
                while(ptridx->next!=NULL){
                    ptridx = ptridx->next;
                }
                ptridx->next = colsoverlap;
                maph5mpi[bidxoverlap].mpirr->nb += 1;
                nbvalproc[pidx] += (rowsidxh5[bidx][1] - rowsidxh5[bidx][0] + 1) * nbcolsoverlapext;
            }
        }

        /* allocate the tile overlap over rows on the next H5 file just below */
        //if (rank == 0) printf("overlap row %d %d %d\n", bidx, h5fileloc[bidx][0], h5fileloc[bidx][1]);
        if ( h5fileloc[bidx][0] < blocknumber1d-1 &&
             h5fileloc[bidx][1] > h5fileloc[bidx][0] && nbrowsh5remain[bidx] == 0 )
        {
            hsize_t nbrowsoverlapint = (idxh5glob[h5fileloc[bidx][0]][1]+1)%tilesize;
            if (nbrowsoverlapint > 0) {
                int bidxoverlap = bidx+blocknumber1d-(h5fileloc[bidx][0]+1);
                hsize_t nbrowsoverlapext = tilesize - nbrowsoverlapint;
                MpiRanks *mpiranksoverlap = malloc( sizeof(MpiRanks) );
                mpiranksoverlap->rank = pidx;
                mpiranksoverlap->next = NULL;
                MpiRanks *ptrrank=maph5mpi[bidxoverlap].mpirr->firstmpiranks;
                while(ptrrank->next!=NULL){
                    ptrrank = ptrrank->next;
                }
                ptrrank->next = mpiranksoverlap;
                IdxInt *rowsoverlap = malloc( sizeof(IdxInt) );
                rowsoverlap->idx[0] = 0;
                rowsoverlap->idx[1] = nbrowsoverlapext-1;
                rowsoverlap->next = NULL;
                IdxInt *ptridx = maph5mpi[bidxoverlap].mpirr->firstrows;
                while(ptridx->next!=NULL){
                    ptridx = ptridx->next;
                }
                ptridx->next = rowsoverlap;
                IdxInt *colsoverlap = malloc( sizeof(IdxInt) );
                colsoverlap->idx[0] = colsidxh5[bidxoverlap][0];
                colsoverlap->idx[1] = colsidxh5[bidxoverlap][1];
                colsoverlap->next = NULL;
                ptridx = maph5mpi[bidxoverlap].mpirr->firstcols;
                while(ptridx->next!=NULL){
                    ptridx = ptridx->next;
                }
                ptridx->next = colsoverlap;
                maph5mpi[bidxoverlap].mpirr->nb += 1;
                nbvalproc[pidx] += nbrowsoverlapext *(colsidxh5[bidxoverlap][1] - colsidxh5[bidxoverlap][0] + 1);
                //nbrowsh5remain[bidxoverlap] -= nbrowsoverlapext;
                nbrowsh5remainglob -= nbrowsoverlapext;
                //if (rank == 0) printf("remove nbrowsh5remainglob from %d nbrowsoverlapext %lld\n", bidxoverlap, nbrowsoverlapext);
            }
            //if (rank == 0) printf("nbrowsoverlapint %lld %lld %lld\n", nbrowsoverlapint, idxh5glob[h5fileloc[bidx][0]][1], nbrowsh5remainglob);
        }

        /* allocate the tile overlap on the next H5 file bottom/right corner */
        //if (rank == 0) printf("overlap bottom/right %d %d %d\n", bidx, h5fileloc[bidx][0], h5fileloc[bidx][1]);
        if ( h5fileloc[bidx][1] < blocknumber1d-1 &&
             h5fileloc[bidx][0] < blocknumber1d-1 &&
             h5fileloc[bidx][1] > h5fileloc[bidx][0] && nbrowsh5remain[bidx] == 0 )
        {
            hsize_t nbrowsoverlapint = (idxh5glob[h5fileloc[bidx][0]][1]+1)%tilesize;
            hsize_t nbcolsoverlapint = (idxh5glob[h5fileloc[bidx][1]][1]+1)%tilesize;
            if (nbrowsoverlapint > 0 && nbcolsoverlapint) {
                int bidxoverlap = bidx+blocknumber1d-(h5fileloc[bidx][0]+1)+1;
                hsize_t nbrowsoverlapext = tilesize - nbrowsoverlapint;
                hsize_t nbcolsoverlapext = tilesize - nbcolsoverlapint;
                MpiRanks *mpiranksoverlap = malloc( sizeof(MpiRanks) );
                mpiranksoverlap->rank = pidx;
                mpiranksoverlap->next = NULL;
                MpiRanks *ptrrank=maph5mpi[bidxoverlap].mpirr->firstmpiranks;
                while(ptrrank->next!=NULL){
                    ptrrank = ptrrank->next;
                }
                ptrrank->next = mpiranksoverlap;
                IdxInt *rowsoverlap = malloc( sizeof(IdxInt) );
                rowsoverlap->idx[0] = 0;
                rowsoverlap->idx[1] = nbrowsoverlapext-1;
                rowsoverlap->next = NULL;
                IdxInt *ptridx = maph5mpi[bidxoverlap].mpirr->firstrows;
                while(ptridx->next!=NULL){
                    ptridx = ptridx->next;
                }
                ptridx->next = rowsoverlap;
                IdxInt *colsoverlap = malloc( sizeof(IdxInt) );
                colsoverlap->idx[0] = 0;
                colsoverlap->idx[1] = nbcolsoverlapext-1;
                colsoverlap->next = NULL;
                ptridx = maph5mpi[bidxoverlap].mpirr->firstcols;
                while(ptridx->next!=NULL){
                    ptridx = ptridx->next;
                }
                ptridx->next = colsoverlap;
                maph5mpi[bidxoverlap].mpirr->nb += 1;
                nbvalproc[pidx] += nbrowsoverlapext * nbcolsoverlapint;
                //nbrowsh5remain[bidxoverlap] -= nbrowsoverlapext;
                //nbrowsh5remainglob -= nbrowsoverlapext;
                //if (rank == 0) printf("remove nbrowsh5remainglob from %d nbrowsoverlapext %lld\n", bidxoverlap, nbrowsoverlapext);
            }
        }
        rowsidxh5[bidx][0] += nbrowsproctoadd;
    }
    for (int i=0; i < nblocks; i++){
        MpiRanks *ptrrank = maph5mpi[i].mpirr->firstmpiranks;
        maph5mpi[i].mpirr->firstmpiranks = ptrrank->next;
        free(ptrrank);
        IdxInt *ptridx = maph5mpi[i].mpirr->firstrows;
        maph5mpi[i].mpirr->firstrows = ptridx->next;
        free(ptridx);
        ptridx = maph5mpi[i].mpirr->firstcols;
        maph5mpi[i].mpirr->firstcols = ptridx->next;
        free(ptridx);
        //if (rank == 0){
        //    printf("%s %d\n", maph5mpi[i].filename, maph5mpi[i].mpirr->nb);
        //    MpiRanks *mpiranksprint = maph5mpi[i].mpirr->firstmpiranks;
        //    IdxInt *rowsprint = maph5mpi[i].mpirr->firstrows;
        //    IdxInt *colsprint = maph5mpi[i].mpirr->firstcols;
        //    for (int j=0; j < maph5mpi[i].mpirr->nb; j++){
        //        printf("%d %lld %lld %lld %lld\n", mpiranksprint->rank, rowsprint->idx[0], rowsprint->idx[1], colsprint->idx[0], colsprint->idx[1]);
        //        mpiranksprint = mpiranksprint->next;
        //        rowsprint = rowsprint->next;
        //        colsprint = colsprint->next;
        //    }
        //}
    }
    MPI_Barrier(MPI_COMM_WORLD);

    /* prepare data for subsets reading */
    hid_t **memspace_id = (hid_t **)malloc(nblocks*sizeof(hid_t*));
    hsize_t start[DIMS];
    hsize_t stride[DIMS];
    hsize_t count[DIMS];
    hsize_t block[DIMS];
    hsize_t dimsloc = 0;
    hsize_t nsubblocks = 0;
    double ***dset_data = (double ***)malloc( nblocks*sizeof(double**) );

    float elapsed_cumul = 0.;
    for (int i=0; i < nblocks; i++){
        nsubblocks += maph5mpi[i].mpirr->nb;
        memspace_id[i] = (hid_t *)malloc(maph5mpi[i].mpirr->nb*sizeof(hid_t));
        dset_data[i] = (double **)malloc( maph5mpi[i].mpirr->nb*sizeof(double*) );
        MpiRanks *mpiranks = maph5mpi[i].mpirr->firstmpiranks;
        IdxInt *rows = maph5mpi[i].mpirr->firstrows;
        IdxInt *cols = maph5mpi[i].mpirr->firstcols;
        for (int j=0; j < maph5mpi[i].mpirr->nb; j++){
            if ( mpiranks->rank == rank ){
                /* Read dataset's values */
                start[0]  = rows->idx[0];
                start[1]  = cols->idx[0];
                stride[0] = 1;
                stride[1] = 1;
                count[0]  = rows->idx[1]-rows->idx[0]+1;
                count[1]  = cols->idx[1]-cols->idx[0]+1;
                block[0]  = 1;
                block[1]  = 1;
                printf("%d: read H5 file %s - subset %lld %lld %lld %lld\n", rank, maph5mpi[i].filename, start[0], start[1], count[0], count[1]);
                /* create memspace */
                memspace_id[i][j] = H5Screate_simple (DIMS, count, NULL);
                /* select subset */
                H5Sselect_hyperslab (dspace_id[i], H5S_SELECT_SET, start, stride, count, block);
                /* allocate buffer to receive data */
                hsize_t buffersize = count[0]*count[1];
                dimsloc += buffersize;
                dset_data[i][j] = (double *)malloc( buffersize*sizeof(double) );
                if (!dset_data[i][j]){
                    printf("%d: allocation of dset_data[%d][%d] has failed\n", rank, i, j);
                    MPI_Finalize();
                    exit(-1);
                }
                struct timeval tstop, tstart;
                float elapsed;
                gettimeofday(&tstart, NULL);
                H5Dread(dataset_id[i], H5T_IEEE_F64LE, memspace_id[i][j], dspace_id[i], H5P_DEFAULT, &dset_data[i][j][0]);
                gettimeofday(&tstop, NULL);
                elapsed = timedifference_msec(tstart, tstop);
                elapsed_cumul += elapsed;
                //printf("%d: H5Dread - Took : %f s\n", rank, elapsed/1000);
                //printf("%d: 1st values: %f %f\n", rank, dset_data[i][j][0], dset_data[i][j][1]);
            }
            mpiranks = mpiranks->next;
            rows = rows->next;
            cols = cols->next;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    printf("%d: Number of elements to read %lld - Balanced number %lld\n", rank, dimsloc, nbvalperprocref);
    hsize_t dimslocsum = 0;
    MPI_Reduce(&dimsloc, &dimslocsum, 1, MPI_UINT64_T, MPI_SUM, 0, MPI_COMM_WORLD);
    hsize_t dimsglobdiff = dimsglob - dimslocsum;
    if ( rank == 0 ) printf("%d: Check number of values to read %lld - %lld = %lld. Number of sub-blocks %lld\n", rank, dimsglob, dimslocsum, dimsglobdiff, nsubblocks);

    printf("%d: H5Dread cumulated - Took : %f s\n", rank, elapsed_cumul/1000);

    for (int i=0; i < nblocks; i++){
        /* Close the H5 dataspace. */
        H5Sclose (dspace_id[i]);
        /* Close the H5 dataset. */
        H5Dclose(dataset_id[i]);
        /* Close the H5 file. */
        H5Fclose(file_id[i]);
        /* deallocate structures */
        for (int j=0; j < maph5mpi[i].mpirr->nb; j++){
            MpiRanks *mpiranks = maph5mpi[i].mpirr->firstmpiranks;
            IdxInt *rows = maph5mpi[i].mpirr->firstrows;
            IdxInt *cols = maph5mpi[i].mpirr->firstcols;
            //printf("%d: %d %d %d\n", rank, i, j, mpiranks->rank);
            if ( mpiranks->rank == rank ){
                free(dset_data[i][j]);
            }
            maph5mpi[i].mpirr->firstmpiranks = maph5mpi[i].mpirr->firstmpiranks->next;
            maph5mpi[i].mpirr->firstrows = maph5mpi[i].mpirr->firstrows->next;
            maph5mpi[i].mpirr->firstcols = maph5mpi[i].mpirr->firstcols->next;
            free(mpiranks);
            free(rows);
            free(cols);
        }
        if (maph5mpi[i].mpirr->nb > 0){
            free(dset_data[i]);
            free(memspace_id[i]);
        }
    }
    free(dset_data);
    free(memspace_id);
    /* Close the metadata .txt file */
    fclose(datafile);
    /* MPI finalize */
    MPI_Finalize();
    /* return status */
    return 0;
}
/**
 * Find maximum between two numbers.
 */
int max(int num1, int num2)
{
    return (num1 > num2 ) ? num1 : num2;
}
/**
 * Find minimum between two numbers.
 */
int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}
float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}