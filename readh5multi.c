/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads HDF5 files with several MPI processes.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <libgen.h>
#include <hdf5.h>
#include <mpi.h>

#define MAXCHAR 1000
#define DIMS 2

int max(int num1, int num2);
int min(int num1, int num2);
float timedifference_msec(struct timeval t0, struct timeval t1);

int main(int argc, char *argv[]) {
    int rank, nprocs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (argc < 2){
        if (rank == 0){
            printf("Please give: \n");
            printf("1. the path to an hdf5 metadata file .txt\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    char h5filenamesg[MAXCHAR*nprocs];
    hsize_t startg[DIMS*nprocs], strideg[DIMS*nprocs], countg[DIMS*nprocs], blockg[DIMS*nprocs];

    if (rank == 0){
        char *filename = malloc(strlen(argv[1]) + 1);
        if (filename != NULL) {
            strcpy(filename, argv[1]);
        } else {
            MPI_Finalize();
            exit(-1);
        }

        /* Read the metadata */
        printf("%d: read txt file %s\n", rank, filename);
        FILE *datafile = fopen(filename, "r");
        if (datafile == NULL){
            printf("Could not open file %s", filename);
            return 1;
        }

        char str[MAXCHAR];
        fgets(str, MAXCHAR, datafile);
        // number of blocks, in rows and columns, e.g. 3 means the global matrix is
        // made of 3x3 blocks
        int blocknumber1d = atoi(str);
        //printf("%d: blocknumber1d %d\n", rank, blocknumber1d);
        fgets(str, MAXCHAR, datafile);
        // upper/lower part of the matrix the blocks represent
        char format[MAXCHAR];
        strcpy(format, str);
        //printf("%d: format %s", rank, format);
        int nblocks = blocknumber1d*(blocknumber1d+1)/2;
        if( format == "full" ){
            nblocks = blocknumber1d*blocknumber1d;
        }
        printf("%d: nblocks %d\n", rank, nblocks);
        if ( nblocks > nprocs ){
            printf("%d: Number of files to read is larger than number of processes, please provide more processes\n", rank);
        }
        nblocks = min(nblocks, nprocs);
        //printf("%d: nblocks %d\n", rank, nblocks);

        int nprocperfile = nprocs / nblocks;
        nprocperfile = max(nprocperfile, 1);
        printf("%d: nprocperfile %d\n", rank, nprocperfile);

        /* Read HDF5 files metadata */
        char **h5filenames = malloc(nblocks * sizeof(char *));
        const char delimiter[] = "\t";
        for (int i=0; i < nblocks; i++){
            /* Read metadata */
            fgets(str, MAXCHAR, datafile);
            char *tmp = strtok(str, delimiter);
            int index_i = atoi(tmp);
            tmp = strtok(NULL, delimiter);
            int index_j = atoi(tmp);
            tmp = strtok(NULL, delimiter);
            h5filenames[i] = (char *)malloc(MAXCHAR);
            strcpy(h5filenames[i], tmp);
            //printf("%d: %d %d %s\n", rank, index_i, index_j, h5filenames[i]);

            /* conversion filename to absolute path expecting H5 files in the
            same directory than the txt file */
            tmp = strdup(filename);
            char *pathtofile = dirname(tmp);
            //printf("%d: read H5 file in %s\n", rank, pathtofile);
            char myfile[MAXCHAR];
            strcpy(myfile, pathtofile);
            strcat(myfile, "/");
            strcat(myfile, h5filenames[i]);
            strcpy(h5filenames[i], myfile);

            /* Reading header to get dimensions. */
            //printf("%d: read H5 file header %s\n", rank, myfile);
            hid_t file_id = H5Fopen(h5filenames[i], H5F_ACC_RDONLY, H5P_DEFAULT);

            /* Get dimensions. */
            hid_t dataset_id = H5Dopen2(file_id, "/distance", H5P_DEFAULT);
            hid_t dspace_id = H5Dget_space(dataset_id);
            hsize_t dims[DIMS];
            H5Sget_simple_extent_dims(dspace_id, dims, NULL);
            //printf("%d: dims %llu %llu\n", rank, dims[0], dims[1]);

            /* Close the H5 dataset. */
            H5Dclose(dataset_id);
            /* Close the H5 file. */
            H5Fclose(file_id);

            /* Define the data structure to read for each process */
            hsize_t offsetrows = 0;
            hsize_t nrows = dims[0] / nprocperfile;
            hsize_t ncols = dims[1];
            for (int j=0; j < nprocperfile; j++){
                /* define filename to open for each process */
                strcpy(h5filenamesg+(i*nprocperfile+j)*MAXCHAR, h5filenames[i]);
                //printf("%d: %d %d %s\n", rank, i, j, h5filenames[i]);
                /* HDF5 parameters for loading a specific block */
                if ( j == nprocperfile-1 ){
                    nrows = dims[0] - j*nrows;
                }
                int indexr = (i*nprocperfile+j)*DIMS;
                int indexc = (i*nprocperfile+j)*DIMS + 1;
                startg[indexr]  = offsetrows;
                startg[indexc]  = 0;
                strideg[indexr] = 1;
                strideg[indexc] = 1;
                countg[indexr]  = nrows;
                countg[indexc]  = ncols;
                blockg[indexr]  = 1;
                blockg[indexc]  = 1;
                offsetrows += nrows;
            }
        }
        /* Deallocate h5filenames */
        for (int i=0; i < nblocks; i++){
            free(h5filenames[i]);
        }
        free(h5filenames);
        /* Close the metadata .txt file */
        fclose(datafile);
    }

    char h5filename[MAXCHAR];
    //printf("%d: %s\n", rank, &h5filenamesg[rank*MAXCHAR]);
    MPI_Scatter(h5filenamesg, MAXCHAR, MPI_CHAR, h5filename, MAXCHAR, MPI_CHAR, 0, MPI_COMM_WORLD);
    //printf("%d: read H5 file header %s\n", rank, h5filename);
    /* parameters to select the subset */
    hsize_t start[DIMS], stride[DIMS], count[DIMS], block[DIMS];
    /* HDF5 parameters for loading a specific block */
    MPI_Scatter(startg, DIMS, MPI_UNSIGNED_LONG, start, DIMS, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
    MPI_Scatter(strideg, DIMS, MPI_UNSIGNED_LONG, stride, DIMS, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
    MPI_Scatter(countg, DIMS, MPI_UNSIGNED_LONG, count, DIMS, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
    MPI_Scatter(blockg, DIMS, MPI_UNSIGNED_LONG, block, DIMS, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
    //printf("%d: subset selection start %lld %lld\n", rank, start[0], start[1]);
    //printf("%d: subset selection stride %lld %lld\n", rank, stride[0], stride[1]);
    //printf("%d: subset selection count %lld %lld\n", rank, count[0], count[1]);
    //printf("%d: subset selection block %lld %lld\n", rank, block[0], block[1]);

    if( access( h5filename, F_OK ) != -1 ) {
        /* Open H5 File */
        hid_t file_id = H5Fopen(h5filename, H5F_ACC_RDONLY, H5P_DEFAULT);
        /* Open Dataset distance */
        hid_t dataset_id = H5Dopen2(file_id, "/distance", H5P_DEFAULT);
        /* create memspace */
        hid_t memspace_id = H5Screate_simple (DIMS, count, NULL);
        /* get dataspace of the dataset */
        hid_t dspace_id = H5Dget_space(dataset_id);
        // select subset
        H5Sselect_hyperslab (dspace_id, H5S_SELECT_SET, start, stride, count, block);

        /* Read dataset's values */
        hsize_t buffersize = count[0]*count[1];
        double *dset_data = (double*) malloc( buffersize*sizeof(double) );
        if (dset_data){
            printf("%d: read H5 file %s - size %lld %lld\n", rank, h5filename, count[0], count[1]);
            struct timeval tstop, tstart;
            float elapsed;
            gettimeofday(&tstart, NULL);
            H5Dread(dataset_id, H5T_IEEE_F64LE, memspace_id, dspace_id, H5P_DEFAULT, dset_data);
            gettimeofday(&tstop, NULL);
            elapsed = timedifference_msec(tstart, tstop);
            printf("%d: H5Dread - Took : %f s\n", rank, elapsed/1000);
            printf("%d: 1st values: %f %f\n", rank, dset_data[0], dset_data[1]);
            free(dset_data);
        }

        /* Close the H5 dataspace. */
        H5Sclose (dspace_id);
        /* Close the H5 dataset. */
        H5Dclose(dataset_id);
        /* Close the H5 file. */
        H5Fclose(file_id);
    }

    /* MPI finalize */
    MPI_Finalize();
    /* return status */
    return 0;
}
/**
 * Find maximum between two numbers.
 */
int max(int num1, int num2)
{
    return (num1 > num2 ) ? num1 : num2;
}
/**
 * Find minimum between two numbers.
 */
int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}
float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}