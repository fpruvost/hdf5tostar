/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads an PnetCDF file first in sequential (on rank 0) then in
 *  parallel MPI+threads by sub-blocks of the matrix using Chameleon.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#include <pnetcdf.h>
#include <starpu.h>
#include <chameleon.h>

static void handle_error(int status, int lineno)
{
    fprintf(stderr, "Error at line %d: %s\n", lineno, ncmpi_strerror(status));
    MPI_Abort(MPI_COMM_WORLD, 1);
}
static int chamMapPnetcdfTile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    );
int min(int num1, int num2);
int max(int num1, int num2);

int main(int argc, char *argv[]) {
    char         *file;
    int          i, j, rank, nprocs, status, nthreads, fileid, distanceid, nb, check;
    int          *fileidthd;
    const int    ndims=2;
    int          dimids[ndims];
    MPI_Offset   dims[ndims];
    MPI_Offset   *offset, *count;
    double       *dset_data=NULL, *dset_data_cm=NULL, *dset_data_diff=NULL, *dset_data_cham=NULL, *block=NULL;
    CHAM_desc_t  *cham_desc=NULL;
    clock_t      start, stop;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (argc < 5){
        if (rank == 0){
            printf("Please give: \n");
            printf("1. the path to an pnetcdf file,\n");
            printf("2. the number of threads\n");
            printf("3. the tile size to use for chameleon\n");
            printf("4. 0/1 without/with checking.\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    file = malloc(strlen(argv[1]) + 1);
    if (file != NULL) {
        strcpy(file, argv[1]);
    } else {
        MPI_Finalize();
        exit(-1);
    }
    nthreads = atoi(argv[2]);
    nb       = atoi(argv[3]);
    check    = atoi(argv[4]);

    printf("Reading file: %s\n", file);

    /* Open an existing file. */
    status = ncmpi_open(MPI_COMM_WORLD, file, NC_NOWRITE, MPI_INFO_NULL, &fileid);
    if (status != NC_NOERR) handle_error(status, __LINE__);
    for(i=0; i<ndims; i++)  {
        status = ncmpi_inq_dimlen(fileid, i, &(dims[i]) );
        if (status != NC_NOERR) handle_error(status, __LINE__);
    }
    if (rank == 0) printf("Dimensions %lld %lld\n", dims[0], dims[1]);
    offset = (MPI_Offset*) calloc(ndims, sizeof(MPI_Offset));
    count  = (MPI_Offset*) calloc(ndims, sizeof(MPI_Offset));
    dset_data = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
    status = ncmpi_inq_varid(fileid, "distance", &distanceid);
    if (status != NC_NOERR) handle_error(status, __LINE__);

    if ( rank==0 ){

        // offset[0] = 0; offset[1] = 0;
        // count[0] = nb; count[1] = nb;
        // block = (double*) malloc( count[0]*count[1]*sizeof(double) );
        // status = ncmpi_get_vara_double_all(fileid, distanceid, offset, count, block);
        // free(block);
        // offset[0] = 0; offset[1] = nb;
        // count[0] = nb; count[1] = dims[1]-nb;
        // block = (double*) malloc( count[0]*count[1]*sizeof(double) );
        // status = ncmpi_get_vara_double_all(fileid, distanceid, offset, count, block);
        // free(block);

        offset[0] = 0; offset[1] = 0;
        count[0] = dims[0]; count[1] = dims[1];
        start = clock();
        status = ncmpi_get_vara_double_all(fileid, distanceid, offset, count, dset_data);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        stop = clock();
        printf("Sequential Pnetcdf - Took : %f s\n", (double)(stop - start) / CLOCKS_PER_SEC);
        printf("1st values: %f %f\n", dset_data[0], dset_data[1]);

    }

    status = ncmpi_close(fileid);
    if (status != NC_NOERR) handle_error(status, __LINE__);

    /* Read dataset with Chameleon (by tiles) */
    printf("Number of threads: %d\n", nthreads);

    fileidthd    = (int*) malloc( nthreads*sizeof(int) );
    for (i=0; i<nthreads; i++){
        status = ncmpi_open(MPI_COMM_WORLD, file, NC_NOWRITE, MPI_INFO_NULL, &(fileidthd[i]));
        if (status != NC_NOERR) handle_error(status, __LINE__);
    }

    /* init chameleon and read hdf5 data with map */
    CHAMELEON_Init( nthreads, 0 );
    CHAMELEON_Set( CHAMELEON_TILE_SIZE, nb );
    CHAMELEON_Desc_Create(&cham_desc, NULL, ChamRealDouble, nb, nb, nb*nb, dims[0], dims[1], 0, 0, dims[0], dims[1], 1, 1);
    start = clock();
    CHAMELEON_map_Tile( ChamUpperLower, cham_desc, chamMapPnetcdfTile, fileidthd );
    stop = clock();
    printf("Multithread Chameleon one file - Took : %f s\n", (double)(stop - start) / CLOCKS_PER_SEC);
    start = clock();
    dset_data_cham = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
    CHAMELEON_Desc2Lap(ChamUpperLower, cham_desc, dset_data_cham, dims[0]);
    printf("1st values: %f %f\n", dset_data_cham[0], dset_data_cham[1]);
    CHAMELEON_Desc_Destroy( &cham_desc );
    CHAMELEON_Finalize();

    for (i=0; i<nthreads; i++){
        status = ncmpi_close(fileidthd[i]);
        if (status != NC_NOERR) handle_error(status, __LINE__);
    }

    // Matrices comparison
    if ( rank == 0  && check==1 ){
      double dmax = 0.;
      double vmax = 0.;
      double epsilon = 1e-14;
      unsigned int n = dims[0]*dims[1];
      // Turn in column major
      dset_data_cm = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
      dset_data_diff = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
      for (j = 0; j < dims[1]; ++j){
          for (i = 0; i < dims[0]; ++i){
              dset_data_cm[ j*dims[0] + i ] = dset_data[ i*dims[1] + j ];
          }
      }
      for (i = 0 ; i < n ; i++){
        dmax = max(dmax, fabs(dset_data_cm[i] - dset_data_cham[i]));
        dset_data_diff[i] = fabs(dset_data_cm[i] - dset_data_cham[i]);
        if (dset_data_diff[i]>epsilon){
            j = i%dims[0];
            printf("%d %d %f\n", i/dims[0], j, dset_data_diff[i]);
        }
        vmax = max(vmax, fabs(dset_data_cm[i]));
      }
      free(dset_data_cm);
      free(dset_data_diff);
      printf("Diff Max %f\n", dmax);
      printf("Val Max %f\n", vmax);
      printf("Epsilon %f\n", epsilon);
      double check = dmax / (vmax*dims[0]*epsilon);
      printf("Check %f\n", check);
      if ( check > 1.0 ){
        printf("Matrices comparison failed\n");
        MPI_Finalize();
        exit(-1);
      } else {
        printf("Matrices comparison succeed\n");
      }
    }

    free(fileidthd);
    free(dset_data);
    free(dset_data_cham);
    free(file);
    MPI_Finalize();

    return status;
}

static int chamMapPnetcdfTile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    ){
    // parameters for tile dimension and position in the global matrix
    size_t nrows, ncols, ldam, row_min, col_min;
    // int parameters of pnetcdf
    int status, distanceid;
    // parameters to select the subset
    MPI_Offset start[2], count[2];
    // we receive data from Pnetcdf in row major format
    double *tilerm;
    // address of the tile to fill
    double *T = (double*)cham_tile->mat;
    // metadata to copy the dataset blocks (pointer to the dataset_id)
    int *fileidthd = (int*)user_data;
    int fileid;

    int starpu_worker_id = starpu_worker_get_id();
    fileid = fileidthd[starpu_worker_id];

    // uplo is meaningfull only for tiles on the diagonal, else we need to copy
    // the entire tile. For now only ChamUpperLower is compatible in this
    // function. TODO: handle Upper and Lower storage.
    uplo = ( m == n ) ? uplo : ChamUpperLower;

    // sizes of the tile
    nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
    ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
    ldam  = descA->get_blkldd( descA, m );
    // starting row index of the block to copy from A
    row_min = m * descA->mb;
    // starting column index of the block to copy from A
    col_min = n * descA->nb;

    // Pnetcdf parameters for loading a specific block
    start[0] = row_min;
    start[1] = col_min;
    count[0] = nrows;
    count[1] = ncols;
    //printf("start[0] %lld, start[1] %lld, count[0] %lld, count[1] %lld\n", start[0], start[1], count[0], count[1]);

    tilerm = (double*) malloc( nrows*ncols*sizeof(double) );

    status = ncmpi_inq_varid(fileid, "distance", &distanceid);
    if (status != NC_NOERR) handle_error(status, __LINE__);

    // read and copy the block into the tile in row major (HDF5 convention)
    //status = ncmpi_get_vara_double_all(fileid, distanceid, start, count, T);
    status = ncmpi_get_vara_double_all(fileid, 0, start, count, tilerm);
    if (status != NC_NOERR) handle_error(status, __LINE__);

    // Turn in column major
    for (size_t j = 0; j < ncols; ++j){
        size_t imin = ( uplo == ChamLower ) ? j               : 0;
        size_t imax = ( uplo == ChamUpper ) ? min(j+1, nrows) : nrows;
        for (size_t i = imin; i < imax; ++i){
            T[ j*ldam + i ] = tilerm[ i*ncols + j ];
        }
    }

    free(tilerm);

    return 0;
}


/**
 * Find minimum between two numbers.
 */
int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}
/**
 * Find maximum between two numbers.
 */
int max(int num1, int num2)
{
    return (num1 > num2 ) ? num1 : num2;
}