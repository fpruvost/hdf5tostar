/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads an HDF5 file gien as parameter.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <hdf5.h>

int main(int argc, char *argv[]) {

    hid_t       file_id, dataset_id;  /* identifiers */
    herr_t      status;
    char        *FILE;
    double      *dset_data;
    clock_t     start, stop;

    if (argc < 2){
        printf("Please give the path to a HDF5 file as argument to this executable\n");
        return 0;
    }

    FILE = malloc(strlen(argv[1]) + 1);
    if (FILE != NULL) {
        strcpy(FILE, argv[1]);
    } else {
        return -1;
    }

    printf("Reading file: %s\n", FILE);

    /* Open an existing file. */
    file_id = H5Fopen(FILE, H5F_ACC_RDWR, H5P_DEFAULT);

    /* Open an existing dataset. */
    dataset_id = H5Dopen2(file_id, "/distance", H5P_DEFAULT);

    hid_t dspace = H5Dget_space(dataset_id);
    const int ndims = H5Sget_simple_extent_ndims(dspace);
    hsize_t dims[ndims];
    H5Sget_simple_extent_dims(dspace, dims, NULL);
    printf("Dims: %llu %llu\n", dims[0], dims[1]);

    /* Read dataset's values */
    dset_data = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
    start = clock();
    status = H5Dread(dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, dset_data);
    stop = clock();
    printf("Took : %f s\n", (double)(stop - start) / CLOCKS_PER_SEC);
    printf("1st values: %f %f\n", dset_data[0], dset_data[1]);

    /* Close the dataset. */
    status = H5Dclose(dataset_id);

    /* Close the file. */
    status = H5Fclose(file_id);

    free(FILE);
    free(dset_data);

    return status;
}
