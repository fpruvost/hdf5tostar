/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads HDF5 files with several MPI processes.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <libgen.h>
#include <hdf5.h>
#include <mpi.h>

#define MAXCHAR 1000
#define DIMS 2
#define INTERVAL 2
#define TOLERANCE 5

int max(int num1, int num2);
int min(int num1, int num2);
float timedifference_msec(struct timeval t0, struct timeval t1);

typedef struct MpiRanks MpiRanks;
/**
 * @brief Mpi ranks handling H5 File
 */
struct MpiRanks{
    int rank;
    MpiRanks *next;
};
typedef struct Rows Rows;
/**
 * @brief Rows intervals handled by proc in H5 Files
 */
struct Rows{
    hsize_t rows[INTERVAL];
    Rows *next;
};
typedef struct MapH5MpiList MapH5MpiList;
struct MapH5MpiList{
    int nb;
    MpiRanks *firstmpiranks;
    Rows *firstrows;
};
/**
 * @brief Define the subset of MPI processes handling the HDF5 file.
 */
struct MapH5Mpi{
    char filename[MAXCHAR]; // H5 filename, absolute path
    int npmax;              // number max of different processes handling the file
    MapH5MpiList *mpirr; // list of MPI ranks handling the file and of rows interval to handle in the file for each MPI rank
};

int main(int argc, char *argv[]) {
    int rank, nprocs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (argc < 2){
        if (rank == 0){
            printf("Please give: \n");
            printf("1. the path to an hdf5 metadata file .txt\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    char *filename = malloc(strlen(argv[1]) + 1);
    if (filename != NULL) {
        strcpy(filename, argv[1]);
    } else {
        MPI_Finalize();
        exit(-1);
    }

    /* Read the metadata */
    printf("%d: read txt file %s\n", rank, filename);
    FILE *datafile = fopen(filename, "r");
    if (datafile == NULL){
        printf("Could not open file %s", filename);
        MPI_Finalize();
        exit(-1);
    }

    char str[MAXCHAR];
    fgets(str, MAXCHAR, datafile);
    // number of blocks, in rows and columns, e.g. 3 means the global matrix is
    // made of 3x3 blocks
    int blocknumber1d = atoi(str);
    //printf("%d: blocknumber1d %d\n", rank, blocknumber1d);
    fgets(str, MAXCHAR, datafile);
    // upper/lower part of the matrix the blocks represent
    char format[MAXCHAR];
    strcpy(format, str);
    //printf("%d: format %s", rank, format);
    int nblocks = blocknumber1d*(blocknumber1d+1)/2;
    if( format == "full" ){
        nblocks = blocknumber1d*blocknumber1d;
    }
    printf("%d: nblocks %d\n", rank, nblocks);

    char h5filenames[nblocks][MAXCHAR];
    hid_t file_id[nblocks];
    hid_t dataset_id[nblocks];
    hid_t dspace_id[nblocks];
    hsize_t dims[nblocks][DIMS];

    /* Read HDF5 files metadata to get dimensions */
    const char delimiter[] = "\t";
    for (int i=0; i < nblocks; i++){
        /* Read metadata */
        fgets(str, MAXCHAR, datafile);
        char *tmp = strtok(str, delimiter);
        int index_i = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        int index_j = atoi(tmp);
        tmp = strtok(NULL, delimiter);
        strcpy(&h5filenames[i][0], tmp);
        //printf("%d: %d %d %s\n", rank, index_i, index_j, h5filenames[i]);
        /* conversion filename to absolute path expecting H5 files in the
        same directory than the txt file */
        tmp = strdup(filename);
        char *pathtofile = dirname(tmp);
        //printf("%d: read H5 file in %s\n", rank, pathtofile);
        char myfile[MAXCHAR];
        strcpy(myfile, pathtofile);
        strcat(myfile, "/");
        strcat(myfile, &h5filenames[i][0]);
        strcpy(&h5filenames[i][0], myfile);
        /* Reading header to get dimensions. */
        //printf("%d: read H5 file header %s\n", rank, &h5filenames[i][0]);
        if( access( &h5filenames[i][0], F_OK ) != -1 ) {
            file_id[i] = H5Fopen(&h5filenames[i][0], H5F_ACC_RDONLY, H5P_DEFAULT);
            /* Get dimensions. */
            dataset_id[i] = H5Dopen2(file_id[i], "/distance", H5P_DEFAULT);
            dspace_id[i] = H5Dget_space(dataset_id[i]);
            //printf("%d: file_id %ld dataset_id %ld dspace_id %ld\n", rank, file_id[i], dataset_id[i], dspace_id[i]);
            H5Sget_simple_extent_dims(dspace_id[i], &dims[i][0], NULL);
            //printf("%d: dims[%d] %llu %llu\n", rank, i, dims[i][0], dims[i][1]);
        } else {
            printf("Could not access file %s", &h5filenames[i][0]);
            MPI_Finalize();
            exit(-1);
        }
    }
    free(filename);

    /* Compute total number of elements over all the blocks. */
    hsize_t dimsglob = 0;
    for (int i=0; i<nblocks; i++){
        dimsglob += dims[i][0] * dims[i][1];
    }
    if (rank == 0) printf("%d: dimsglob %llu\n", rank, dimsglob);

    hsize_t nbvalperprocref = dimsglob / nprocs ;
    hsize_t nbvaltolerance = nbvalperprocref*TOLERANCE/(float)100;
    if (rank == 0) printf("%d: Average number of elements per process so that it is balanced %llu tolerance +- %llu\n", rank, nbvalperprocref, nbvaltolerance);
    if (nbvalperprocref == 0){
        printf("Matrix is so small that we don't have the minimum of one value per process to read, please decrease the number of processes or consider a larger matrix.\n");
        MPI_Finalize();
        exit(-1);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    /* Hdf5 files <-> MPI ranks allocation */

    /* Initialize structures */
    struct MapH5Mpi maph5mpi[nblocks];
    for (int i=0; i<nblocks; i++){
        strcpy(maph5mpi[i].filename, &h5filenames[i][0]);
        maph5mpi[i].npmax = 1;
        maph5mpi[i].mpirr = malloc( sizeof(MapH5MpiList) );
        maph5mpi[i].mpirr->nb = 0;
        MpiRanks *mpiranks = malloc( sizeof(MpiRanks) );
        mpiranks->rank = 0;
        mpiranks->next = NULL;
        maph5mpi[i].mpirr->firstmpiranks = mpiranks;
        Rows *rows = malloc( sizeof(Rows) );
        rows->rows[0] = 0;
        rows->rows[1] = 0;
        rows->next = NULL;
        maph5mpi[i].mpirr->firstrows = rows;
    }
    hsize_t nbrowsh5remain[nblocks]; // number of remaining rows to handle in each file
    hsize_t nbrowsh5remainglob = 0; // number of remaining rows considering all files
    hsize_t rowsminh5[nblocks]; // index of rows where to start during allocations
    hsize_t rowsmaxh5[nblocks]; // max index of rows during allocations
    for (int i=0; i<nblocks; i++){
        nbrowsh5remain[i] = dims[i][0];
        nbrowsh5remainglob += dims[i][0];
        rowsminh5[i] = 0;
        rowsmaxh5[i] = -1;
    }
    hsize_t nbvalproc[nprocs]; // load on each proc
    for (int i=0; i<nprocs; i++){
        nbvalproc[i] = 0;
    }

    /* Main loop for allocation */
    while ( nbrowsh5remainglob > 0 ){

        /* search the most loaded H5 block */
        int bidx = 0;
        hsize_t blockloadmax = nbrowsh5remain[0]*dims[0][1];
        for (int i=1; i < nblocks; i++){
            hsize_t blockload = nbrowsh5remain[i]*dims[i][1];
            if ( blockload > blockloadmax ){
                blockloadmax = blockload;
                bidx = i;
            }
        }

        /* search the least loaded proc */
        int pidx = 0;
        hsize_t procloadmin = nbvalproc[0];
        for (int i=1; i < nprocs; i++){
            hsize_t procload = nbvalproc[i];
            if ( procload < procloadmin){
                procloadmin = procload;
                pidx = i;
            }

        }

        /* load proc with H5 block rows */
        hsize_t nbrowstofillproc = max(1, (nbvalperprocref - nbvalproc[pidx]) / dims[bidx][1] + 1);
        hsize_t nbrowsproctoadd  = min(nbrowsh5remain[bidx], nbrowstofillproc);

        /* if remaining number of rows is very small we accept to handle it to
        avoid getting new processes handling this file */
        if ( (nbrowsh5remain[bidx] - nbrowsproctoadd)*dims[bidx][1] < nbvaltolerance ){
            nbrowsproctoadd = nbrowsh5remain[bidx];
        }
        if (rank == 0) printf("%d %d %lld %lld %lld %lld\n", bidx, pidx, nbrowsh5remain[bidx], nbvalproc[pidx], nbrowstofillproc, nbrowsproctoadd);

        nbvalproc[pidx]      += nbrowsproctoadd * dims[bidx][1];
        nbrowsh5remain[bidx] -= nbrowsproctoadd;
        nbrowsh5remainglob   -= nbrowsproctoadd;
        rowsmaxh5[bidx]      += nbrowsproctoadd;

        /* new allocation for this H5 block */
        MpiRanks *mpiranks = malloc( sizeof(MpiRanks) );
        mpiranks->rank = pidx;
        mpiranks->next = maph5mpi[bidx].mpirr->firstmpiranks;
        maph5mpi[bidx].mpirr->firstmpiranks = mpiranks;
        Rows *rows = malloc( sizeof(Rows) );
        rows->rows[0] = rowsminh5[bidx];
        rows->rows[1] = rowsmaxh5[bidx];
        rows->next = maph5mpi[bidx].mpirr->firstrows;
        maph5mpi[bidx].mpirr->firstrows = rows;
        maph5mpi[bidx].mpirr->nb += 1;

        rowsminh5[bidx] += nbrowsproctoadd;

        if (rank == 0){
            printf("%s %d\n", maph5mpi[bidx].filename, maph5mpi[bidx].mpirr->nb);
            MpiRanks *mpiranksprint = maph5mpi[bidx].mpirr->firstmpiranks;
            Rows *rowsprint = maph5mpi[bidx].mpirr->firstrows;
            for (int j=0; j < maph5mpi[bidx].mpirr->nb; j++){
                printf("%d %lld %lld \n", mpiranksprint->rank, rowsprint->rows[0], rowsprint->rows[1]);
                mpiranksprint = mpiranksprint->next;
                rowsprint = rowsprint->next;
            }
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);

    /* prepare data for subsets reading */
    hid_t **memspace_id = (hid_t **)malloc(nblocks*sizeof(hid_t*));
    hsize_t start[DIMS];
    hsize_t stride[DIMS];
    hsize_t count[DIMS];
    hsize_t block[DIMS];
    hsize_t dimsloc = 0;
    double ***dset_data = (double ***)malloc( nblocks*sizeof(double**) );

    float elapsed_cumul = 0.;
    for (int i=0; i < nblocks; i++){
        memspace_id[i] = (hid_t *)malloc(maph5mpi[i].mpirr->nb*sizeof(hid_t));
        dset_data[i] = (double **)malloc( maph5mpi[i].mpirr->nb*sizeof(double*) );
        MpiRanks *mpiranks = maph5mpi[i].mpirr->firstmpiranks;
        Rows *rows = maph5mpi[i].mpirr->firstrows;
        for (int j=0; j < maph5mpi[i].mpirr->nb; j++){
            if ( mpiranks->rank == rank ){
                /* Read dataset's values */
                start[0]  = rows->rows[0];
                start[1]  = 0;
                stride[0] = 1;
                stride[1] = 1;
                count[0]  = rows->rows[1]-rows->rows[0]+1;
                count[1]  = dims[i][1];
                block[0]  = 1;
                block[1]  = 1;
                /* create memspace */
                memspace_id[i][j] = H5Screate_simple (DIMS, count, NULL);
                /* select subset */
                H5Sselect_hyperslab (dspace_id[i], H5S_SELECT_SET, start, stride, count, block);
                /* allocate buffer to receive data */
                hsize_t buffersize = count[0]*count[1];
                dimsloc += buffersize;
                dset_data[i][j] = (double *)malloc( buffersize*sizeof(double) );
                if (!dset_data[i][j]){
                    printf("%d: allocation of dset_data[%d][%d] has failed\n", rank, i, j);
                    MPI_Finalize();
                    exit(-1);
                }
                printf("%d: read H5 file %s - subset %lld %lld %lld %lld\n", rank, maph5mpi[i].filename, start[0], start[1], count[0], count[1]);
                struct timeval tstop, tstart;
                float elapsed;
                gettimeofday(&tstart, NULL);
                H5Dread(dataset_id[i], H5T_IEEE_F64LE, memspace_id[i][j], dspace_id[i], H5P_DEFAULT, &dset_data[i][j][0]);
                gettimeofday(&tstop, NULL);
                elapsed = timedifference_msec(tstart, tstop);
                elapsed_cumul += elapsed;
                //printf("%d: H5Dread - Took : %f s\n", rank, elapsed/1000);
                //printf("%d: 1st values: %f %f\n", rank, dset_data[i][j][0], dset_data[i][j][1]);
            }
            mpiranks = mpiranks->next;
            rows = rows->next;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    printf("%d: Number of elements to read %lld - Balanced number %lld\n", rank, dimsloc, nbvalperprocref);
    printf("%d: H5Dread cumulated - Took : %f s\n", rank, elapsed_cumul/1000);

    for (int i=0; i < nblocks; i++){
        /* Close the H5 dataspace. */
        H5Sclose (dspace_id[i]);
        /* Close the H5 dataset. */
        H5Dclose(dataset_id[i]);
        /* Close the H5 file. */
        H5Fclose(file_id[i]);
        /* deallocate structures */
        for (int j=0; j < maph5mpi[i].mpirr->nb; j++){
            MpiRanks *mpiranks = maph5mpi[i].mpirr->firstmpiranks;
            Rows *rows = maph5mpi[i].mpirr->firstrows;
            //printf("%d: %d %d %d\n", rank, i, j, mpiranks->rank);
            if ( mpiranks->rank == rank ){
                free(dset_data[i][j]);
            }
            maph5mpi[i].mpirr->firstmpiranks = maph5mpi[i].mpirr->firstmpiranks->next;
            maph5mpi[i].mpirr->firstrows = maph5mpi[i].mpirr->firstrows->next;
            free(mpiranks);
            free(rows);
        }
        if (maph5mpi[i].mpirr->nb > 0){
            free(dset_data[i]);
            free(memspace_id[i]);
        }
    }
    free(dset_data);
    free(memspace_id);
    /* Close the metadata .txt file */
    fclose(datafile);
    /* MPI finalize */
    MPI_Finalize();
    /* return status */
    return 0;
}
/**
 * Find maximum between two numbers.
 */
int max(int num1, int num2)
{
    return (num1 > num2 ) ? num1 : num2;
}
/**
 * Find minimum between two numbers.
 */
int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}
float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}