/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads an HDF5 file then dump data into a new pnetcdf file.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <hdf5.h>
#include <mpi.h>
#include <pnetcdf.h>

static void handle_error(int status, int lineno)
{
    fprintf(stderr, "Error at line %d: %s\n", lineno, ncmpi_strerror(status));
    MPI_Abort(MPI_COMM_WORLD, 1);
}

int main(int argc, char *argv[]) {
    hid_t       file_id, dataset_id, dataspace_id;  /* identifiers */
    herr_t      status;
    int         rank, nprocs, ncfile, dimrowid, dimcolid, distanceid;
    const int   ndims=2, issym=1;
    hsize_t     dims[ndims];
    int         dimsid[ndims];
    MPI_Offset  offset[ndims], count[ndims];
    char        *FILEIN, *FILEOUT, *dataset;
    double      *dset_data;
    clock_t     start, stop;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (argc < 4){
        if (rank == 0) {
            printf("Please give:\n");
            printf("1. the path to an existing HDF5 file\n");
            printf("2. the path to the pnetcdf file to create\n");
            printf("3. the name of the dataset to read\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    FILEIN  = malloc(strlen(argv[1]) + 1);
    if (FILEIN != NULL) {
        strcpy(FILEIN, argv[1]);
    } else {
        return -1;
    }
    FILEOUT  = malloc(strlen(argv[2]) + 1);
    if (FILEOUT != NULL) {
        strcpy(FILEOUT, argv[2]);
    } else {
        return -1;
    }
    dataset  = malloc(strlen(argv[3]) + 1);
    if (dataset != NULL) {
        strcpy(dataset, argv[3]);
    } else {
        return -1;
    }

    if (rank==0){

        printf("Reading file: %s\n", FILEIN);

        /* Open an existing hdf5 file. */
        file_id = H5Fopen(FILEIN, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Open an existing dataset. */
        dataset_id = H5Dopen2(file_id, dataset, H5P_DEFAULT);

        hid_t dspace = H5Dget_space(dataset_id);
        H5Sget_simple_extent_dims(dspace, dims, NULL);
        printf("Dims: %llu %llu\n", dims[0], dims[1]);

        /* Read dataset's values */
        dset_data = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
        start = clock();
        status = H5Dread(dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, dset_data);
        stop = clock();
        printf("Took : %f s\n", (double)(stop - start) / CLOCKS_PER_SEC);
        printf("1st values: %f %f\n", dset_data[0], dset_data[1]);

        /* Close the dataset. */
        status = H5Dclose(dataset_id);

        /* Close the file. */
        status = H5Fclose(file_id);

    }

    /* Create a new pnetcdf file and write data into */
    if (rank==0){

        printf("Writing file: %s\n", FILEOUT);

        status = ncmpi_create(MPI_COMM_WORLD, FILEOUT,
                              (NC_NETCDF4 || NC_CLASSIC_MODEL), MPI_INFO_NULL, &ncfile);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        status = ncmpi_def_dim(ncfile, "nrows", dims[0], &dimrowid);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        status = ncmpi_def_dim(ncfile, "ncols", dims[1], &dimcolid);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        dimsid[0] = dimrowid;
        dimsid[1] = dimcolid;
        status = ncmpi_def_var(ncfile, dataset, NC_DOUBLE, ndims, dimsid, &distanceid);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        status = ncmpi_put_att_int(ncfile, distanceid, "issymmetric", NC_INT, 1, &issym);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        status = ncmpi_enddef(ncfile);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        offset[0] = 0; offset[1] = 0;
        count[0] = dims[0]; count[1] = dims[1];
        status = ncmpi_put_vara_double_all(ncfile, distanceid, offset, count, dset_data);
        if (status != NC_NOERR) handle_error(status, __LINE__);
        status = ncmpi_close(ncfile);
        if (status != NC_NOERR) handle_error(status, __LINE__);

    }

    free(FILEIN);
    free(FILEOUT);
    free(dataset);
    free(dset_data);

    MPI_Finalize();

    return status;
}
