/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads an HDF5 file first in sequential then in parallel by
 *  sub-blocks of the matrix using Chameleon.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <hdf5.h>
#include <mpi.h>
#include <chameleon.h>

static int chamMapHdf5ReadTile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    );
static int chamMapHdf5WriteTile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    );
int min(int num1, int num2);
float timedifference_msec(struct timeval t0, struct timeval t1);

int main(int argc, char *argv[]) {
    hid_t        file_id, dataset_id, memspace_id, dspace_id, plist_id;  /* identifiers */
    hid_t        file_id_thd, dataset_id_thd;
    const int    ndims=2;
    hsize_t      dims[ndims], maxdims[ndims], chunk_dims[ndims], offset[ndims], stride[ndims], count[ndims], block[ndims];
    herr_t       status;
    int          rank, nprocs, nb, nthreads;
    char         *file, *filew;
    double       *dset_data=NULL;
    CHAM_desc_t  *cham_desc=NULL;
    RUNTIME_sequence_t *sequence = NULL;
    RUNTIME_request_t *request = NULL;
    struct timeval stop, start;
    float elapsed;

    maxdims[0] = H5S_UNLIMITED;
    maxdims[1] = H5S_UNLIMITED;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (argc < 4){
        if (rank == 0){
            printf("Please give: \n");
            printf("1. the path to an hdf5 file\n");
            printf("2. the number of threads\n");
            printf("3. the tile size\n");
        }
        MPI_Finalize();
        exit(-1);
    }

    file = malloc(strlen(argv[1]) + 1);
    if (file != NULL) {
        strcpy(file, argv[1]);
    } else {
        MPI_Finalize();
        exit(-1);
    }
    filew = malloc(strlen(file) + 4);
    strcpy(filew, file);
    strcat(filew, ".par");

    nthreads = atoi(argv[2]);

    nb = atoi(argv[3]);

    if (rank == 0) printf("Reading file: %s\n", file);

    if (rank == 0){
        /* Open an existing file. */
        file_id = H5Fopen(file, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Open an existing dataset. */
        dataset_id = H5Dopen2(file_id, "/distance", H5P_DEFAULT);
        dspace_id = H5Dget_space(dataset_id);
        H5Sget_simple_extent_dims(dspace_id, dims, NULL);
        printf("Dims: %llu %llu\n", dims[0], dims[1]);
    }

    MPI_Bcast(dims, ndims, MPI_UINT64_T, 0, MPI_COMM_WORLD);
    dset_data = (double*) malloc( dims[0]*dims[1]*sizeof(double) );

    if (rank == 0){
        /* Close the dataset. */
        status = H5Dclose(dataset_id);

        /* Close the file. */
        status = H5Fclose(file_id);
    }

    /* Read dataset with Chameleon (by tiles, using h5 subset selection) */
    if (rank == 0) printf("Number of threads: %d\n", nthreads);

    /* open file and dataset */
    file_id_thd    = H5Fopen(file, H5F_ACC_RDONLY, H5P_DEFAULT);
    dataset_id_thd = H5Dopen2(file_id_thd, "/distance", H5P_DEFAULT);

    /* init chameleon and read hdf5 data with map */
    CHAMELEON_Init( nthreads, 0 );
    CHAMELEON_Set( CHAMELEON_TILE_SIZE, nb );
    if (rank == 0) printf("Chameleon tile size is %d\n", nb);
    CHAMELEON_Desc_Create(&cham_desc, NULL, ChamRealDouble, nb, nb, nb*nb, dims[0], dims[1], 0, 0, dims[0], dims[1], nprocs, 1);

    CHAMELEON_Sequence_Create(&sequence);
    CHAMELEON_Request_Create(&request);
    CHAMELEON_Request_Set(request, CHAMELEON_REQUEST_WORKERID, 0);

    gettimeofday(&start, NULL);
    CHAMELEON_map_Tile_Async( ChamUpperLower, cham_desc, chamMapHdf5ReadTile, &dataset_id_thd, sequence, request );
    CHAMELEON_Sequence_Wait(sequence);
    gettimeofday(&stop, NULL);
    elapsed = timedifference_msec(start, stop);
    if (rank == 0) printf("Multithread Chameleon one file - Took : %f ms\n", elapsed);

    H5Dclose(dataset_id_thd);
    H5Fclose(file_id_thd);

    /*
     * Set up file access property list with parallel I/O access
     */
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    status = H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    /*
     * Create a new file collectively and release property list identifier.
     */
    file_id = H5Fcreate(filew, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    H5Pclose(plist_id);

    /*
     * Create the dataspace for the dataset.
     */
    chunk_dims[0] = nb;
    chunk_dims[1] = nb;
    dspace_id = H5Screate_simple(ndims, dims, maxdims);
    // memspace_id = H5Screate_simple(ndims, chunk_dims, maxdims);

    /*
     * Create chunked dataset.
     */
    plist_id = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_chunk(plist_id, ndims, chunk_dims);
    /* Set ZLIB / DEFLATE Compression using compression level 5. Not available
    in parallel MPI ... */
    //H5Pset_deflate(plist_id, 5);
    dataset_id = H5Dcreate(file_id, "/distance", H5T_IEEE_F64LE,
                           dspace_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);
    H5Pclose(plist_id);
    H5Sclose(dspace_id);

    if (dataset_id >= 0){
        gettimeofday(&start, NULL);
        CHAMELEON_map_Tile_Async( ChamUpperLower, cham_desc, chamMapHdf5WriteTile, &dataset_id, sequence, request );
        CHAMELEON_Sequence_Wait(sequence);
        gettimeofday(&stop, NULL);
        elapsed = timedifference_msec(start, stop);
        if (rank == 0) printf("Multithread Chameleon write file - Took : %f ms\n", elapsed);
        H5Dclose(dataset_id);
    }

    H5Fclose(file_id);

    CHAMELEON_Request_Destroy(request);
    CHAMELEON_Sequence_Destroy(sequence);

    CHAMELEON_Desc2Lap(ChamUpperLower, cham_desc, dset_data, dims[0]);
    if (rank == 0) printf("1st values: %f %f\n", dset_data[0], dset_data[1]);

    CHAMELEON_Desc_Destroy( &cham_desc );
    CHAMELEON_Finalize();

    free(dset_data);
    free(file);
    free(filew);

    MPI_Finalize();

    return status;
}

static int chamMapHdf5ReadTile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    ){
    // parameters for tile dimension and position in the global matrix
    size_t nrows, ncols, ldam, row_min, col_min;
    // parameters to select the subset
    hsize_t start[2], stride[2], count[2], block[2];
    // we receive data from HDF5 in row major format
    double *tilerm;
    // address of the tile to fill
    double *T = (double*)cham_tile->mat;
    // metadata to copy the dataset blocks (pointer to the dataset_id)
    hid_t *dset = (hid_t*)user_data;

    // uplo is meaningfull only for tiles on the diagonal, else we need to copy
    // the entire tile. For now only ChamUpperLower is compatible in this
    // function. TODO: handle Upper and Lower storage.
    uplo = ( m == n ) ? uplo : ChamUpperLower;

    // sizes of the tile
    nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
    ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
    ldam  = descA->get_blkldd( descA, m );
    // starting row index of the block to copy from A
    row_min = m * descA->mb;
    // starting column index of the block to copy from A
    col_min = n * descA->nb;

    // HDF5 parameters for loading a specific block
    start[0] = row_min;
    start[1] = col_min;
    stride[0] = 1;
    stride[1] = 1;
    count[0] = nrows;
    count[1] = ncols;
    block[0] = 1;
    block[1] = 1;

    // create memspace
    hid_t memspace = H5Screate_simple (2, count, NULL);
    // get dataspace of the dataset
    hid_t dspace = H5Dget_space(*dset);
    // select subset
    H5Sselect_hyperslab (dspace, H5S_SELECT_SET, start, stride, count, block);

    // read and copy the block into the tile in row major (HDF5 convention)
    tilerm = (double*) malloc( nrows*ncols*sizeof(double) );
    H5Dread (*dset, H5T_IEEE_F64LE, memspace, dspace, H5P_DEFAULT, tilerm);

    // Turn in column major
    for (size_t j = 0; j < ncols; ++j){
        size_t imin = ( uplo == ChamLower ) ? j               : 0;
        size_t imax = ( uplo == ChamUpper ) ? min(j+1, nrows) : nrows;
        for (size_t i = imin; i < imax; ++i){
            T[ j*ldam + i ] = tilerm[ i*ncols + j ];
        }
    }

    free(tilerm);
    H5Sclose (dspace);

    return 0;
}

static int chamMapHdf5WriteTile(
    const CHAM_desc_t *descA,
    cham_uplo_t uplo, int m, int n,
    CHAM_tile_t *cham_tile, void *user_data
    ){
    // parameters for tile dimension and position in the global matrix
    size_t nrows, ncols, ldam, row_min, col_min;
    // h5 parameters to select the subset
    hsize_t start[2], stride[2], count[2], block[2], chunk[2];
    // h5 property list identifier
    hid_t plist_id;
    // we receive data from HDF5 in row major format
    double *tilerm;
    // address of the tile to fill
    double *T = (double*)cham_tile->mat;
    // metadata to copy the dataset blocks (pointer to the dataset_id)
    hid_t *dset = (hid_t*)user_data;

    // uplo is meaningfull only for tiles on the diagonal, else we need to copy
    // the entire tile. For now only ChamUpperLower is compatible in this
    // function. TODO: handle Upper and Lower storage.
    uplo = ( m == n ) ? uplo : ChamUpperLower;

    // sizes of the tile
    nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
    ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
    ldam  = descA->get_blkldd( descA, m );
    // starting row index of the block to copy from A
    row_min = m * descA->mb;
    // starting column index of the block to copy from A
    col_min = n * descA->nb;

    // HDF5 parameters for loading a specific block
    start[0] = row_min;
    start[1] = col_min;
    stride[0] = 1;
    stride[1] = 1;
    //count[0] = nrows;
    //count[1] = ncols;
    //block[0] = 1;
    //block[1] = 1;
    count[0] = 1;
    count[1] = 1;
    block[0] = nrows;
    block[1] = ncols;
    chunk[0] = descA->mb;
    chunk[1] = descA->nb;

    // create memspace
    hid_t memspace = H5Screate_simple (2, block, NULL);
    // get dataspace of the dataset
    hid_t dspace = H5Dget_space(*dset);
    // select subset
    H5Sselect_hyperslab (dspace, H5S_SELECT_SET, start, stride, count, block);

    // read and copy the block into the tile in row major (HDF5 convention)
    tilerm = (double*) malloc( nrows*ncols*sizeof(double) );
    // Turn in row major
    for (size_t j = 0; j < ncols; ++j){
        size_t imin = ( uplo == ChamLower ) ? j               : 0;
        size_t imax = ( uplo == ChamUpper ) ? min(j+1, nrows) : nrows;
        for (size_t i = imin; i < imax; ++i){
            tilerm[ i*ncols + j ] = T[ j*ldam + i ];
        }
    }

    /*
     * Create property list for collective dataset write.
     */
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_INDEPENDENT);

    H5Dwrite(*dset, H5T_IEEE_F64LE, memspace, dspace, plist_id, tilerm);

    H5Pclose(plist_id);
    H5Sclose(dspace);
    free(tilerm);

    return 0;
}

/**
 * Find minimum between two numbers.
 */
int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}

float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}
