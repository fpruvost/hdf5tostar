/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2020 Inria                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This program reads an HDF5 file then dump data into a new netcdf file.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <hdf5.h>
#include <netcdf.h>

/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

int main(int argc, char *argv[]) {
    hid_t       file_id, dataset_id, dataspace_id;  /* identifiers */
    herr_t      status;
    int         ncfile, dimrowid, dimcolid, distanceid;
    const int   ndims=2, issym=1;
    hsize_t     dims[ndims];
    int         dimsid[ndims];
    char        *FILEIN, *FILEOUT, *dataset;
    double      *dset_data;
    clock_t     start, stop;

    if (argc < 4){
        printf("Please give:\n");
        printf("1. the path to an existing HDF5 file\n");
        printf("2. the path to the netcdf file to create\n");
        printf("3. the name of the dataset to read\n");
        exit(-1);
    }

    FILEIN  = malloc(strlen(argv[1]) + 1);
    if (FILEIN != NULL) {
        strcpy(FILEIN, argv[1]);
    } else {
        return -1;
    }
    FILEOUT  = malloc(strlen(argv[2]) + 1);
    if (FILEOUT != NULL) {
        strcpy(FILEOUT, argv[2]);
    } else {
        return -1;
    }
    dataset  = malloc(strlen(argv[3]) + 1);
    if (dataset != NULL) {
        strcpy(dataset, argv[3]);
    } else {
        return -1;
    }

    printf("Reading file: %s\n", FILEIN);
    /* Open an existing hdf5 file. */
    file_id = H5Fopen(FILEIN, H5F_ACC_RDONLY, H5P_DEFAULT);
    /* Open an existing dataset. */
    dataset_id = H5Dopen2(file_id, dataset, H5P_DEFAULT);
    hid_t dspace = H5Dget_space(dataset_id);
    H5Sget_simple_extent_dims(dspace, dims, NULL);
    printf("Dims: %llu %llu\n", dims[0], dims[1]);
    /* Read dataset's values */
    dset_data = (double*) malloc( dims[0]*dims[1]*sizeof(double) );
    start = clock();
    status = H5Dread(dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, dset_data);
    stop = clock();
    printf("Took : %f s\n", (double)(stop - start) / CLOCKS_PER_SEC);
    printf("1st values: %f %f\n", dset_data[0], dset_data[1]);
    /* Close the dataset. */
    status = H5Dclose(dataset_id);
    /* Close the file. */
    status = H5Fclose(file_id);

    /* Create a new netcdf file and write data into */
    printf("Writing file: %s\n", FILEOUT);
    status = nc_create(FILEOUT, NC_CLOBBER, &ncfile);
    if (status!=0) ERR(status);
    status = nc_def_dim(ncfile, "nrows", dims[0], &dimrowid);
    if (status!=0) ERR(status);
    status = nc_def_dim(ncfile, "ncols", dims[1], &dimcolid);
    if (status!=0) ERR(status);
    dimsid[0] = dimrowid;
    dimsid[1] = dimcolid;
    status = nc_def_var(ncfile, dataset, NC_DOUBLE, ndims, dimsid, &distanceid);
    if (status!=0) ERR(status);
    status = nc_enddef(ncfile);
    if (status!=0) ERR(status);
    status = nc_put_var_double(ncfile, distanceid, dset_data);
    if (status!=0) ERR(status);
    status = nc_close(ncfile);
    if (status!=0) ERR(status);

    free(FILEIN);
    free(FILEOUT);
    free(dataset);
    free(dset_data);

    return status;
}
